<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $fillable = [
    'nombre_autor', 'apellido_autor',
    ];

    public function libros()
    {
    	return $this->hasMany('\App\Libro');
    }
}
