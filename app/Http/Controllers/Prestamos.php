<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Prestamo;
use Carbon\Carbon;
use \App\Libro;
use \App\Alert;

class Prestamos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mes)
    {
        if($mes == 1)
        {
            $inicio_mes = Carbon::now()->startOfMonth();
            $today      = Carbon::now();
            $prestamos = Prestamo::where('created_at', '>', $inicio_mes)->where('created_at', '<', $today)->get();
        }   
        else
        {
            $prestamos = Prestamo::all();
        }

        return view('layouts.prestamo')->with(['prestamos' => $prestamos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'   => 'required|max:20',
            'apellido' => 'required|max:40',
            'telefono' => 'required|numeric',
            'id'       => 'required',
            ]);

        $prestamo           = new Prestamo();
        $prestamo->nombre   = $request->nombre;
        $prestamo->apellido = $request->apellido;
        $prestamo->telefono = $request->telefono;
        $prestamo->libro_id = $request->id;
        $prestamo->updated_at = null;

        $libro = Libro::find($request->id);
        $libro->disponible = false;

        if ($prestamo->save() && $libro->save()) {
            Alert::alert('Se registró el prestamo', 'success');
        }else{
            Alert::alert('Error al resgistrar el prestamo');
        }
        return back();
    }

    public function regresar(Request $request)
    {
        $this->validate($request, [
            'id_prestamo' => 'required',
            'contrasena' => 'required',
            'id_libro' => 'required',
            ]);

        if ( \Hash::check(trim($request->contrasena), auth()->user()->password ) ) 
        {
            $libro = Libro::find($request->id_libro);
            $libro->disponible = true;

            $prestamo = Prestamo::find($request->id_prestamo);
            $prestamo->touch();

            if($libro->save() && $prestamo->save())
            {
                Alert::alert('Libro devuelto', 'success');
            }else 
            {
                Alert::alert('Error de conexión', 'error');
            }
        }
        else
        {
            Alert::alert('Contraseña inválida', 'error');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
