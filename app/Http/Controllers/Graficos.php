<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\FechaBusqueda;
use \App\Prestamo;
use \App\Busqueda;
use Carbon\Carbon;
use \App\Genero;
use \DB;

class Graficos extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiempo_inicio = $this->microtime_float(); // Inicia cronometro

        $_meses                 = $this->_meses(); // Array con los últimos 12 meses
        $data_busquedas_ano     = $this->_fechas_busquedas($_meses); // Retorna las fechas cuando se realizaron las busquedas
        $data_prestamos_ano     = $this->_fechas_prestamos($_meses); // Retorna las fechas cuando se realizaron los prestamos
        $data_busquedas         = Busqueda::orderBy('cantidad', 'desc')->limit(5)->get(); // Obtiene las 5 palabras más buscadas
        $data_generos_busquedas = Genero::orderBy('busquedas', 'desc')->limit(5)->get(); // Obtiene los 5 géneros más buscados
        $data_libros_prestamos  = $this->_data_libros_prestamos(5); // Retorna una colección de los libros más prestados con información obtenida de los prestamos
        $data_prestamos_generos = $this->_prestamos_generos(); // Obtiene el prestamo id, libros id, genero id, y nombre del genero. Agrupado por nombre de género
        $data_meses             = $_meses;

        $tiempo_final = $this->microtime_float(); // Finaliza cronometro
        $data_micro_time =  round($tiempo_final - $tiempo_inicio, 4);



        return view('layouts.grafico')->with([
            'data_busquedas'         => $data_busquedas,
            'data_generos_busquedas' => $data_generos_busquedas,
            'data_libros_prestamos'  => $data_libros_prestamos,
            'data_prestamos_generos' => $data_prestamos_generos,
            'data_busquedas_ano'     => $data_busquedas_ano,
            'data_prestamos_ano'     => $data_prestamos_ano,
            'data_meses'             => $data_meses,
            'data_micro_time'        => $data_micro_time,
            ]);
    }


    function microtime_float()
    {
        list($useg, $seg) = explode(" ", microtime());
        return ((float)$useg + (float)$seg);
    }

    private function _meses()
    {
        $meses = [];
        $meses[0] = Carbon::now()->startOfMonth()->subMonth(12);
        $meses[1] = Carbon::now()->startOfMonth()->subMonth(11);
        $meses[2] = Carbon::now()->startOfMonth()->subMonth(10);
        $meses[3] = Carbon::now()->startOfMonth()->subMonth(9);
        $meses[4] = Carbon::now()->startOfMonth()->subMonth(8);
        $meses[5] = Carbon::now()->startOfMonth()->subMonth(7);
        $meses[6] = Carbon::now()->startOfMonth()->subMonth(6);
        $meses[7] = Carbon::now()->startOfMonth()->subMonth(5);
        $meses[8] = Carbon::now()->startOfMonth()->subMonth(4);
        $meses[9] = Carbon::now()->startOfMonth()->subMonth(3);
        $meses[10] = Carbon::now()->startOfMonth()->subMonth(2);
        $meses[11] = Carbon::now()->startOfMonth()->subMonth(1);
        $meses[12] = Carbon::now()->startOfMonth();
        // dd($meses);
        return $meses;
    }


    /**
     * [_fechas_prestamos description]
     * @return [type] [description]
     */
    private function _fechas_prestamos($meses)
    {
        $prestamos = Prestamo::where('created_at', '>=', $meses[0])->orderBy('created_at', 'asc')->get();

        $datos = [];
        $cantidad_meses = count($meses);
        for ($i=0; $i < $cantidad_meses; $i++) 
        { 
            if ($i+1 == $cantidad_meses) 
            {
                $collection = $prestamos->where('created_at', '>=', $meses[$i]);
            }
            else
            {
                $collection = $prestamos->where('created_at', '>=', $meses[$i])->where('created_at', '<', $meses[$i+1]);
            }
            array_push($datos, $collection);
        }
        return $datos;
    }


    /**
     * [_fechas_busquedas description]
     * @return array:3 [▼
      0 => Collection {#296 ▼
        #items: array:1 [▼
          0 => Busqueda {#295 ▼
            #fillable: array:1 [▶]
            #connection: null
            #table: null
            #primaryKey: "id"
            #keyType: "int"
            #perPage: 15
            +incrementing: true
            +timestamps: true
            #attributes: array:5 [▼
              "id" => 10
              "created_at" => "2016-10-03 00:00:00"
              "updated_at" => "2016-10-03 00:00:00"
            ]
            #original: array:5 [▶]
            #relations: []
            #hidden: []
            #visible: []
            #appends: []
            #guarded: array:1 [▶]
            #dates: []
            #dateFormat: null
            #casts: []
            #touches: []
            #observables: []
            #with: []
            +exists: true
            +wasRecentlyCreated: false
          }
        ]
      }
      1 => Collection {#294 ▶}
      2 => Collection {#318 ▶}
    ]
     */
    private function _fechas_busquedas($meses)
    {
        $busquedas = FechaBusqueda::where('created_at', '>=', $meses[0])->orderBy('created_at', 'asc')->get();

        // Ordenar datos por mes
        $datos = [];
        $cantidad_meses = count($meses);
        for ($i=0; $i < $cantidad_meses; $i++) 
        { 
            if (($i+1) == $cantidad_meses) 
            {
                $collection = $busquedas->where('created_at', '>=', $meses[$i]);   
            }
            else
            {
                $collection = $busquedas->where('created_at', '>=', $meses[$i])->where('created_at', '<', $meses[$i+1]);   
            }
            array_push($datos, $collection);
        }
        // dd($datos);
        return $datos;
    }


    /**
     * [_prestamos_generos description]
     * @return Collection {#284 ▼
        #items: array:2 [▼
        "Children's Fiction" => Collection {#282 ▼
          #items: array:14 [▼
            0 => {#263 ▼
              +"_prestamo_id": 1
              +"_libro_id": 6
              +"_genero_id": 3
              +"_genero_nombre": "Children's Fiction"
            }
            1 => {#264 ▶}
          ]
        }
        "Play" => Collection {#283 ▼
          #items: array:4 [▼
            0 => {#277 ▼
              +"_prestamo_id": 15
              +"_libro_id": 118
              +"_genero_id": 7
              +"_genero_nombre": "Play"
            }
            1 => {#278 ▶}
          ]
        }
      ]
    }
     */
    private function _prestamos_generos()
    {
        $data = DB::table('prestamos')
        ->join('libros', 'prestamos.libro_id', '=', 'libros.id')
        ->join('generos', 'libros.genero_id', '=', 'generos.id')
        ->select('prestamos.id as _prestamo_id', 'libros.id as _libro_id', 'generos.id as _genero_id', 'generos.nombre as _genero_nombre')
        ->get(); // Selecciona el prestamo id, libros id, genero id, y nombre del genero

        $data = collect($data)->groupBy('_genero_nombre'); // Agrupa los datos por nombre de género

        return $data;
    }

    /**
     * [_data_libros description]
     * @return [type] Collection {#241 ▼
          #items: array:2 [▼
            0 => {#239 ▼
              +"prestamos_count": 7
              +"libro_id": 6
            }
            1 => {#240 ▼
              +"id": 6
              +"titulo": "El juego de las formas"
              +"serie": null
              +"isbn": "9-789681-671846"
              +"disponible": 1
              +"autor_id": 2
              +"genero_id": 3
              +"sub_genero_id": null
              +"created_at": "2016-12-26 22:50:17"
              +"updated_at": "2016-12-27 04:38:13"
            }
          ]
        }
     */
        private function _data_libros_prestamos($cantidad)
        {
            $data_prestamos = $this->_top_prestamos($cantidad); // Selecciona los libros más prestados
            $ids_libros = $this->_ids($data_prestamos); // Selecciona únicamente los ids de los libros

            $data_libro = DB::table('libros')
            ->select('*')
            ->whereIn('id', $ids_libros)
            ->groupBy('id')
            ->get(); // Selecciona los libros

            $merge = $this->_merge($data_prestamos, $data_libro); // Une la información de los prestamos con los datos de los libros (Retorna una colección)
            return $merge;
        }

        private function _top_prestamos($cantidad)
        {
            $data_prestamos = DB::table('prestamos')
            ->select(DB::raw('count(*) as prestamos_count, libro_id'))
            ->where('libro_id', '!=', null)
            ->orderBy('prestamos_count', 'desc')
            ->limit($cantidad)
            ->groupBy('libro_id')
            ->get();

            return $data_prestamos;
        }

        private function _ids($data_prestamos)
        {
            $ids = [];
            foreach ($data_prestamos as $value) 
            {
                array_push($ids, $value->libro_id);
            }
            return $ids;
        }

        private function _merge($data1 , $data2)
        {
            $data1 = collect($data1);
            $data2 = collect($data2);
            $merge = $data1->merge($data2);
            return $merge;
        }


    
}
