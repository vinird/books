<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request; 
use \App\FechaBusqueda;
use \App\Busqueda;
use \App\Genero;
use \App\Libro;
use \App\Alert;
use \App\Autor;

class Libros extends Controller
{
    /**
     * [cargar_info description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function cargar_info(Request $request)       
    {
        $this->validate($request, [
         'archivo' => 'required|mimes:xml,ods,xlsx,xls',
         'contrasena' => 'required',
         ]);
        if ( \Hash::check(trim($request->contrasena), auth()->user()->password ) )
        {
            echo "Cargando datos...";
            $file = $request->file('archivo');

            // Cargar autores ////////////////////

            Excel::load($file, function($reader) {
                foreach ($reader->get() as $autores) {
                    foreach ($autores as $autor) {
                        if($autor->authors_last_name != null){
                            $existe = Autor::where('apellido_autor', '=', $autor->authors_last_name)->first();
                            if($existe == null){
                                $nAutor = new Autor();
                                if (strlen($autor->authors_last_name) < 39) 
                                {
                                    $nAutor->apellido_autor = $autor->authors_last_name;
                                }
                                if(strlen($autor->authors_first_name) < 19)
                                {
                                    $nAutor->nombre_autor = $autor->authors_first_name;
                                }
                                $nAutor->save();
                            }
                        }
                    }
                }
            }, 'UTF-8');

            // Cargar géneros ////////////////////
            
            Excel::load($file, function($reader) {

                foreach ($reader->get() as $generos) {
                    foreach ($generos as $genero) {
                        if($genero->genre != null){
                            $existe = Genero::where('nombre', '=', $genero->genre)->first();
                            if($existe == null && strlen($genero->genre) < 39){
                                $nGenero = new Genero();
                                $nGenero->nombre = $genero->genre;
                                $nGenero->save();
                            }
                        }
                    }
                }
            }, 'UTF-8');

            // Cargar Libros //////////////////////
            
            Excel::load($file, function($reader) {
                foreach ($reader->get() as $libros) 
                {
                    foreach ($libros as $libro) 
                    {
                        if($libro->authors_last_name != null && $libro->title_of_book != null && $libro->isbn != null)
                        {
                            $isbnLibro = Libro::where('isbn', '=', $libro->isbn)->first();
                            if($isbnLibro == null && strlen($libro->title_of_book) < 99 && strlen(trim($libro->isbn)) ){
                                $nLibro = new Libro();
                                $nLibro->titulo = $libro->title_of_book;
                                $nLibro->serie = $libro->series_number;
                                $nLibro->isbn = trim($libro->isbn);
                                $nLibro->disponible = true;

                                // Agregar autor
                                $tempAutor = Autor::where('apellido_autor', '=', $libro->authors_last_name)->first();
                                if($tempAutor != null)
                                {
                                    $nLibro->autor_id = $tempAutor->id;
                                }

                                // Agreagar género 
                                $tempGenero = Genero::where('nombre', '=', $libro->genre)->first();
                                if ($tempGenero != null) 
                                {
                                    $nLibro->genero_id = $tempGenero->id;
                                }

                                // Agregar sub genero
                                $tempSubGenero = Genero::where('nombre', '=', $libro->subgenre)->first();
                                if ($tempSubGenero != null) 
                                {
                                    $nLibro->sub_genero_id = $tempSubGenero->id;
                                }
                                $nLibro->save();
                            }
                            else // Si el libro existe
                            {
                                // Agregar autor
                                $tempAutor = Autor::where('apellido_autor', '=', $libro->authors_last_name)->first();
                                if($tempAutor != null)
                                {
                                    $isbnLibro->autor_id = $tempAutor->id;
                                }

                                // Agreagar género 
                                $tempGenero = Genero::where('nombre', '=', $libro->genre)->first();
                                if ($tempGenero != null) 
                                {
                                    $isbnLibro->genero_id = $tempGenero->id;
                                }

                                // Agregar sub genero
                                $tempSubGenero = Genero::where('nombre', '=', $libro->subgenre)->first();
                                if ($tempSubGenero != null) 
                                {
                                    $isbnLibro->sub_genero_id = $tempSubGenero->id;
                                }
                                $isbnLibro->save();
                            }
                        }
                    }
                }
            }, 'UTF-8');
            Alert::alert('Libros agregados. Se ha actualizado la base de datos', 'success');
        }
        else 
        {
            Alert::alert('Contraseña inválida', 'error');
        }

        return back();
    }

    /**
     * [buscar_libro description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function buscar_libro(Request $request)
    {
        if(auth()->check() == null)
        {
            $this->_mining_buscar_libro($request->text); // Minería de busquedas
        }

        $libros = $this->_buscar_libro($request->text);
        return view('welcome')->with(['libros' => $libros]);
    }


    /**
     * [admin_buscar_libro description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function admin_buscar_libro(Request $request)
    {
        $libros = $this->_buscar_libro($request->text);
        
        return view('home')->with(['libros' => $libros]);
    }

    public function _mining_buscar_libro($text)
    {
        $keys = explode(" " , $text);
        $words = [];
        for ($i=0; $i < count($keys) ; $i++) 
        { 
            if(strlen($keys[$i]) < 20 && strlen($keys[$i]) > 3)
            {
                $words[$i] = strtolower($keys[$i]);
            }
        }
        foreach ($words as $word) 
        {
            $busquedad = Busqueda::where('texto', 'LIKE', '%'.$word.'%')->first(); // Trae el que más se asemeja
            if(count($busquedad) > 0) // Si hay semejantes
            {
                if(strtolower($word) == strtolower($busquedad->texto)) // Son iguales
                {
                    $busquedad->cantidad = $busquedad->cantidad + 1;
                    $busquedad->save();
                } 
                else // Son diferentes 
                {
                    $newBusqueda = new Busqueda();
                    $newBusqueda->texto = $word;
                    $newBusqueda->cantidad = 1;
                    $newBusqueda->save();
                }
            }
            else
            {
                $newBusqueda = new Busqueda();
                $newBusqueda->texto = $word;
                $newBusqueda->cantidad = 1;
                $newBusqueda->save();
            }
        }
        $fechaBusqueda = new FechaBusqueda();
        $fechaBusqueda->save();
    }

    /**
     * [_buscar_libro description]
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
    public function _buscar_libro($text)
    {
        $autors = Autor::where('apellido_autor', 'LIKE', '%'.$text.'%')->orWhere('nombre_autor', 'LIKE', '%'.$text.'%')->get();
        $ids = [];
        foreach ($autors as $autor) 
        {
            array_push($ids, $autor->id);
        }
        $libros2 = Libro::whereIn('autor_id', $ids)->get();
        $libros = Libro::where('titulo', 'LIKE', '%'.$text.'%')->orWhere('isbn', 'LIKE', '%'.$text.'%')->get();
        $merge = $libros->merge($libros2);
        return $merge;
    }

    /**
     * [buscar_libro_genero description]
     * @param  [type] $genero_id [description]
     * @return [type]            [description]
     */
    public function buscar_libro_genero($genero_id)
    {
        if(auth()->check() == null)
        {
            $this->_mining_buscar_libro_genero($genero_id); // Minería de busquedas por género
        }

        $libros = Libro::where('genero_id', '=', $genero_id)->orWhere('sub_genero_id', '=', $genero_id)->get();

        return view('welcome')->with(['libros' => $libros]);
    }

    /**
     * [admin_buscar_libro_genero description]
     * @param  [type] $genero_id [description]
     * @return [type]            [description]
     */
    public function admin_buscar_libro_genero($genero_id)
    {
        $libros = Libro::where('genero_id', '=', $genero_id)->orWhere('sub_genero_id', '=', $genero_id)->get();

        return view('home')->with(['libros' => $libros]);
    }

    public function _mining_buscar_libro_genero($id_genero)
    {
        $genero = Genero::find($id_genero);
        if($genero->busquedas == null)
        {
            $genero->busquedas = 1;
        }
        else
        {
            $genero->busquedas = $genero->busquedas + 1;
        }
        $genero->save();
    }

    /**
     * [libros_all description]
     * @return [type] [description]
     */
    public function libros_all()
    {
        $libros = Libro::all();
        return view('home')->with(['libros' => $libros]);
    }

    /**
     * [libro description]
     * @param  [type] $libro_id [description]
     * @return [type]           [description]
     */
    public function libro($libro_id)       
    {
        $libro   = Libro::find($libro_id);
        return view('layouts.libro')->with(['libro' => $libro]);
    }

    public function show($id)
    {
        $libros = Libro::where('id', '=', $id)->get();
        return view('home')->with(['libros' => $libros]);
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'autor'  => 'required',
           'titulo' => 'required|max:100',
           'isbn'   => 'max:40|unique:libros,isbn',
           'genero' => 'required',
           'serie'  => 'muneric',
           ]);

        $libro             = new Libro();
        $libro->titulo     = $request->titulo;
        $libro->genero_id  = $request->genero;
        $libro->autor_id   = $request->autor;
        $libro->isbn       = trim($request->isbn);
        $libro->disponible = true;

        if ($request->subgenero != null) 
        {
            $libro->sub_genero_id = $request->subgenero;
        }
        if ($request->serie != null) 
        {
            $libro->serie = $request->serie;
        }

        if ($libro->save()) 
        {
            Alert::alert('Libro agregado', 'success');
        }else 
        {
            Alert::alert('Error al agregar el libro', 'error');
        }
        return back();
    }

    /**
     * [delete description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
           'contrasena' => 'required',]);

        if (\Hash::check(trim($request->contrasena), auth()->user()->password)) 
        {
            if(Libro::destroy($request->id))
            {
                Alert::alert('Libro eliminado', 'success');
                return redirect('home');
            }else 
            {
                Alert::alert('Error al eliminar el libro', 'error');
            }
        }else 
        {
            Alert::alert('Contraseña inválida', 'error');
        }
        return back();
    }

    /**
     * [update description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request)
    {
        $this->validate($request, [
           'autor'  => 'required',
           'titulo' => 'required|max:100',
           'genero' => 'required',
           'serie'  => 'muneric',
           ]);

        $libro             = Libro::find($request->id);
        $libro->titulo     = $request->titulo;
        $libro->genero_id  = $request->genero;
        $libro->autor_id   = $request->autor;
        $libro->disponible = true;

        if ($request->subgenero != null) 
        {
            $libro->sub_genero_id = $request->subgenero;
        }
        if ($request->serie != null) 
        {
            $libro->serie      = $request->serie;
        }

        if ($libro->save()) 
        {
            Alert::alert('Libro actualizado', 'success');
        }else 
        {
            Alert::alert('Error al agregar el libro', 'error');
        }
        return back();
    }


}
