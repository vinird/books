<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Genero;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
        // return view('home');
    }

    public function index_cliente()
    {
        $generos = Genero::all();
        return view('welcome')->with(['generos' => $generos]);
    }
}
