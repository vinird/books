<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;
use \App\Alert;

class Users extends Controller
{
	/**
	 * [index description]
	 * @return [type] [description]
	 */
    public function index()
    {
    	$users = User::where('id', '!=', auth()->user()->id)->get();
    	return view('layouts.tabla-usuarios')->with(['users' => $users]);
    }

    /**
     * [block description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function block($id)
    {
    	$user = User::find($id);
    	$user->block = true;
    	$user->solicitud = false;
    	$user->nuevo = false;
    	if ($user->save()) 
    		Alert::alert('Usuario bloqueado', 'success');
    	else
    		Alert::alert('Error de conexión', 'error');
    	return back();
    }

    /**
     * [unblock description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function unblock($id)
    {
    	$user = User::find($id);
    	$user->block = false;
    	$user->solicitud = false;
    	$user->nuevo = false;
    	if ($user->save()) 
    		Alert::alert('Usuario desbloqueado', 'success');
    	else
    		Alert::alert('Error de conexión', 'error');
    	return back();
    }

    /**
     * [enable description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function enable($id)
    {
    	$user = User::find($id);
    	$user->activo = true;
    	$user->solicitud = false;
    	$user->nuevo = false;
    	if ($user->save()) 
    		Alert::alert('Usuario activado', 'success');
    	else
    		Alert::alert('Error de conexión', 'error');
    	return back();
    }

    /**
     * [disable description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function disable($id)
    {
    	$user = User::find($id);
    	$user->activo = false;
    	$user->solicitud = false;
    	$user->nuevo = false;
    	if ($user->save()) 
    		Alert::alert('Usuario desactivado', 'success');
    	else
    		Alert::alert('Error de conexión', 'error');
    	return back();
    }

    /**
     * [delete description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
    	if (User::destroy($id)) 
    		Alert::alert('Usuario eliminado', 'success');
    	else 
    		Alert::alert('Error de conexión', 'error');
    	return back();
    }


    public function request_access()
    {
    	$user = User::find(auth()->user()->id);
    	$user->solicitud = true;
    	if ($user->save()) 
    		Alert::alert('Acceso solicitado', 'success');
    	else
    		Alert::alert('Error de conexión', 'error');
    	return back();
    }

}
