<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use \App\Libro;

class ExcelData extends Controller
{

	/**
	 * [export description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function export(Request $request)
	{
		Excel::create('Biblioteca-'.Carbon::now()->toDateString(), function($excel) {
			// Set the title
			$excel->setTitle('Reporte de libros de la biblioteca ADISL');

			$excel->sheet('Hoja1', function($sheet) {

				$libros = Libro::all();
				$array  = [];

				foreach ($libros as $libro) 
				{
					if ($libro->sub_genero_id == null) 
						$subgenero = null;
					else
						$subgenero = $libro->sub_genero->nombre;

					if ($libro->genero != null)
						$genero = $libro->genero->nombre;
					else
						$genero = null;

					array_push($array,
						["Author's Last Name" => $libro->autor->apellido_autor, 
						"Author's first name" => $libro->autor->nombre_autor,
						"Title of book" => $libro->titulo, 
						"Series number" => $libro->serie,
						"ISBN" => $libro->isbn,
						'Genre' => $genero,
						'Subgenre' => $subgenero,
						]);
				} 

				$sheet->with($array);

				// Set font with ->setStyle()`
				$sheet->setStyle(array(
					'font' => array(
						'name'      =>  'Calibri',
						'size'      =>  10,
						'bold'      =>  false
						)
					));
				$sheet->cells('A1:G1', function($cells) 
				{
					$cells->setFontWeight('bold');
				});
				$sheet->row(1, function($row) {
					$row->setBackground('#C4FFD4');

				});
			});

			$excel->sheet('Hoja2 No borrar', function($sheet) {
				$sheet->with([
					'Datos de la biblioteca.', 
					'Generados en la fecha: '.Carbon::now()->toDateString(), 
					'Por el usuario: '.auth()->user()->name, 
					'Email: '.auth()->user()->email,
					]);
			});

		})->export('xlsx');
		return back();
	}


	public function example()
	{
		Excel::create('Plantilla-Biblioteca-'.Carbon::now()->toDateString(), function($excel) {
			// Set the title
			$excel->setTitle('Reporte de libros de la biblioteca ADISL');

			$excel->sheet('Hoja1', function($sheet) {

				
				$array  = [];

				

				array_push($array,
					["Author's Last Name" => 'Apellido del autor', 
					"Author's first name" => 'Nombre del autor',
					"Title of book" => 'Título del libro', 
					"Series number" => 'Némero de serie',
					"ISBN" => 'ISBN',
					'Genre' => 'Nombre género',
					'Subgenre' => 'Nombre subgenero',
					]);
				

				$sheet->with($array);


				// Set font with ->setStyle()`
				$sheet->setStyle(array(
					'font' => array(
						'name'      =>  'Calibri',
						'size'      =>  10,
						'bold'      =>  false
						)
					));
				$sheet->cells('A1:G1', function($cells) 
				{
					$cells->setFontWeight('bold');
				});
				$sheet->row(1, function($row) {
					$row->setBackground('#C4FFD4');

				});
			});

			$excel->sheet('Hoja2 No borrar', function($sheet) {
				$sheet->with([
					'Datos de la biblioteca.', 
					'Generados en la fecha: '.Carbon::now()->toDateString(), 
					'Por el usuario: '.auth()->user()->name, 
					'Email: '.auth()->user()->email,
					]);
			});



		})->export('xlsx');
		return back();
	}
}
