<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Autor;
use \App\Libro;
use \App\Alert;

class Autores extends Controller
{
	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index()
	{
		return view('layouts.tabla-autores');
	}

	/**
	 * [buscar_autor description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function buscar_autor($id)
	{
		$autor = Autor::find($id);

		return view('layouts.autor')->with(['autor' => $autor]);
	}

	/**
	 * [buscar_libros_autor description]
	 * @param  [type] $id_autor [description]
	 * @return [type]           [description]
	 */
	public function buscar_libros_autor($id_autor)
	{
		$libros  = Libro::where('autor_id', '=', $id_autor)->get();

		return view('home')->with(['libros' => $libros]);
	}

	/**
	 * [store description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'nombre' => 'required|max:20',
			'apellido' => 'required|max:40',
			]);

		$autor = new Autor();
		$autor->nombre_autor = $request->nombre;
		$autor->apellido_autor = $request->apellido;

		if ($autor->save()) 
		{
			Alert::alert('Autor agregado', 'success');
		}
		else
		{
			Alert::alert('Error de conexión', 'error');
		}
		return back();
	}

	/**
	 * [delete description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function delete(Request $request)
	{
		$this->validate($request, [
			'contrasena' => 'required',
			]);

		if (\Hash::check(trim($request->contrasena), auth()->user()->password)) 
		{
			if (Autor::destroy($request->id)) 
			{
				Alert::alert('Autor eliminado', 'success');
				return redirect('admin/autores');
			}
			else
			{
				Alert::alert('Error de conexión', 'error');
			}
		}
		else
		{
			Alert::alert('Contraseña inválida', 'error');
		}
		return back();
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'nombre' => 'required|max:20',
			'apellido' => 'required|max:40',
			]);

		$autor = Autor::find($request->id);
		$autor->nombre_autor = $request->nombre;
		$autor->apellido_autor = $request->apellido;

		if ($autor->save()) 
		{
			Alert::alert('Autor modificado', 'success');
		}
		else
		{
			Alert::alert('Error de conexión', 'error');
		}
		return back();

	}


}
