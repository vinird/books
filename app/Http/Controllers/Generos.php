<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Genero;
use \App\Alert;

class Generos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.tabla-generos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:20']);

        $genero = new Genero();
        $genero->nombre = $request->nombre;

        if ($genero->save()) {
            Alert::alert('Nuevo género agregado', 'success');
        } else {
            Alert::alert('Error al agregar el género', 'error');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genero  = Genero::find($id);

        return view('layouts.genero')->with(['genero' => $genero]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $this->validate($request, ['contrasena' => 'required']);
        $id = $request->id;

        if ( \Hash::check(trim($request->contrasena), auth()->user()->password ) ) 
        {
            $genero = Genero::find($id);
            $cantidad_libros =   $genero->libros_genero->count() +  $genero->libros_sub_genero->count();
            if($cantidad_libros > 0)
            {
                Alert::alert('No se puede eliminar el género porque tiene libros asignados.', 'warning');
                return back();
            }else {
                if(Genero::destroy($id))
                {
                    Alert::alert('Género eliminado', 'success');
                    return redirect('/home');
                } else{
                    Alert::alert('Error al eliminar el género', 'error');
                    return back();
                }
            }
        }else {
            Alert::alert('Contraseña inválida', 'error');
            return back();
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:20']);

        $genero = Genero::find($request->id);
        $genero->nombre = $request->nombre;

        if ($genero->save()) 
        {
            Alert::alert('Género actualizado', 'success');
        }
        else
        {
            Alert::alert('Error de conexión', 'error');
        }
        return back();
    }
}
