<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Token;
use \App\Alert;
use \App\User;

class Tokens extends Controller
{
	/**
	 * [update description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function update(Request $request)
	{
		$this->validate($request, [
			'contrasena' => 'required',
			'token' => 'required', 
			]);
		if ( \Hash::check(trim($request->contrasena), auth()->user()->password ) )
		{
			$token = Token::first();
			$token->token = michaels_encrypt(str_random(30));
			if ($token->save()) 
			{
				Alert::alert('Token actualizado', 'success');
			}
			else
			{
				Alert::alert('Error de conexión', 'error');
			}
		}
		else
		{
			Alert::alert('Contraseña inválida', 'error');
		}
		return back();
	}

	public function check(Request $request)
	{
		$this->validate($request, ['token' => 'required']);

		$token = Token::first();
		if($request->token == michaels_decrypt($token->token))
		{
			$user = User::find(auth()->user()->id);
			$user->activo = true;
			$user->nuevo = false;
			if($user->save())
			{
				$this->touch();
				Alert::alert('Su usuario ha sido activado con éxito', 'success');
				return redirect('/home');
			}
			else
			{
				Alert::alert('Error de conexión', 'error');
				return back();
			}
		}
		else
		{
			Alert::alert('Token inválido', 'error');
			return back();
		}
	}

	/**
	 * [touch description]
	 * @return [type] [description]
	 */
	static function touch()
	{
		$token = Token::first();
		$token->token = michaels_encrypt(str_random(30));
		$token->save(); 
	}
}
