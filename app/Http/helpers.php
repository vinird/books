<?php 

use \App\Autor;
use \App\Genero;
use \App\Token;
use \App\User;

//This function is used to encrypt data.
function michaels_encrypt($text, $salt = "paldeitnsmexyrhd")
{
	return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
}
// This function will be used to decrypt data.
function michaels_decrypt($text, $salt = "paldeitnsmexyrhd")
{
	return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}
// http://earlysandwich.com/programming/simple-php-encrypt-decrypt-functions-81/

function generos()
{
	return Genero::orderBy('nombre')->get();
}

function autores()
{
	return Autor::orderBy('apellido_autor')->get();
}

function token()
{
	return Token::first();
}

function user_notification()
{
	return count(User::where('nuevo', '=', true)->orWhere('solicitud', '=', true)->get());
}
?>