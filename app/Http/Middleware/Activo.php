<?php

namespace App\Http\Middleware;

use Closure;
use \App\Alert;

class Activo
{ 
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->admin == false) {
            if(auth()->user()->block == true)
            {
                Alert::alert('Su usuario ha sido bloqueado.', 'error');
                auth()->logout();
                return redirect('/');
            }
            else
            {
                if (auth()->user()->nuevo == true) 
                {
                    # code... Redirecciona a token
                    return redirect('admin/token/login');
                }
                elseif (auth()->user()->activo == false) 
                {
                    # code... redirecciona a loggin
                    Alert::alert('Su usuario está desactivado.', 'info');
                    
                    return redirect('admin/token/login');
                }
                else
                {
                    return $next($request);
                }
            }
        }
        else
        {
            return $next($request);
        }
    }
}
