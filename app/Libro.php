<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $fillable = [
    'nombre_autor', 'apellido_autor', 'titulo', 'serie', 'isbn', 'disponible', 'genero_id', 'sub_genero_id',
    ];

    public function genero()
    {
    	return $this->belongsTo('\App\Genero', 'genero_id');
    }

    public function sub_genero()
    {
    	return $this->belongsTo('\App\Genero', 'sub_genero_id');
    }

    public function autor()
    {
        return $this->belongsTo('\App\Autor');
    }

    public function prestamos()
    {
        return $this->hasMany('\App\Prestamo');
    }

}
