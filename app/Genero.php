<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    protected $fillable = [
    'nombre',
    ];

    public function libros()
    {
    	return $this->hasMany('\App\Libro');
    }

    public function libros_genero()
    {
    	return $this->hasMany('\App\Libro', 'genero_id');
    }

    public function libros_sub_genero()
    {
    	return $this->hasMany('\App\Libro', 'sub_genero_id');
    }
}
