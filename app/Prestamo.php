<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    protected $fillable = [
    'nombre', 'apellido', 'libro_id', 'telefono',
    ];

    public function libro()
    {
    	return $this->belongsTo('\App\Libro');
    }
}
