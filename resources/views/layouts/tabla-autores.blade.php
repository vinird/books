<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-info">
				<div class="panel-heading">Autores</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table id="tabla_libros" class="table table-hover">
							<thead>
								<th>Nombre</th>
								<th>Cantidad de libros</th>
								<th></th>
							</thead>
							<tbody>
								@if(isset($autors))
								@foreach($autors as $autor)
								<tr class="">
									<td>{{ $autor->apellido_autor.' '.$autor->nombre_autor }}</td>
									<td>
										<?php $cantidad_libros =   $autor->libros->count(); ?>
										<a href="{{ url('admin/buscar_libros_autor/'.$autor->id) }}" >{{ $cantidad_libros }}</a>
									</td>
									<td>
										<a href="{{ url('admin/buscar_autor/'.$autor->id) }}" class="btn btn-default btn-xs">Ver</a>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
