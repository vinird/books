<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-info">
				<div class="panel-heading">Géneros</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table id="tabla_libros" class="table table-hover">
							<thead>
								<th>Nombre</th>
								<th>Cantidad de libros</th>
								<th></th>
							</thead>
							<tbody>
								@if(isset($generos))
								@foreach($generos as $genero)
								<tr class="">
									<td>{{ $genero->nombre }}</td>
									<td>
										<?php $cantidad_libros =   $genero->libros_genero->count() +  $genero->libros_sub_genero->count(); ?>
										<a href="{{ url('admin/buscar_libro_genero/'.$genero->id) }}" >{{ $cantidad_libros }}</a>
									</td>
									<td>
										<a href="{{ url('admin/genero/'.$genero->id) }}" class="btn btn-default btn-xs">Ver</a>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
