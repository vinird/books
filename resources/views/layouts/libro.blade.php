<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			@if(isset($libro))
			<div class="container" >
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							Información del libro
						</div>
						<div class="panel-body">
							<div class="col-xs-12 col-sm-6 col-md-5">
								<form class="form-horizontal" autocomplete="off" action="{{ url('admin/libro/update') }}" method="POST">
									
									{{ csrf_field() }}
									<input type="text" class="hidden" value="{{ $libro->id }}" name="id">
									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Autor</label>
										<div class="col-sm-9">
											<select class="form-control" name="autor" required="">
												<?php $existe = false; ?>
												@foreach($autors as $autor)
												@if($libro->autor_id == $autor->id)
												<option selected="selected" value="{{ $autor->id }}">{{ $autor->apellido_autor.' '.$autor->nombre_autor }}</option>
												<?php $existe = true ?>
												@else
												<option  value="{{ $autor->id }}">{{ $autor->apellido_autor.' '.$autor->nombre_autor }}</option>
												@endif
												@endforeach
												@if($existe == false)
												<option selected="selected"  value="">-- Seleccione un autor --</option>
												@endif
												<?php  $existe = false;?>
											</select>
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Título</label>
										<div class="col-sm-9">
											<input type="text" name="titulo" class="form-control" value="{{ $libro->titulo }}" required="" maxlength="100">
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">ISBN</label>
										<div class="col-sm-9">
											<input type="text" name="isbn" class="form-control" value="{{ $libro->isbn }}" required="" readonly="" maxlength="40">
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Género</label>
										<div class="col-sm-9">
											<select class="form-control" name="genero" required="">
												<?php $existe = false; ?>
												@foreach($generos as $genero)
												@if($libro->genero_id == $genero->id)
												<option selected="selected" value="{{ $genero->id }}">{{ $genero->nombre }}</option>
												<?php $existe = true ?>
												@else
												<option  value="{{ $genero->id }}">{{ $genero->nombre }}</option>
												@endif
												@endforeach
												@if($existe == false)
												<option selected="selected"  value="">-- Seleccione un género --</option>
												@endif
												<?php  $existe = false;?>
											</select>
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Sub-género</label>
										<div class="col-sm-9">
											<select class="form-control" name="subgenero">
												@foreach($generos as $genero)
												@if($libro->sub_genero_id == $genero->id)
												<option selected="selected" value="{{ $genero->id }}">{{ $genero->nombre }}</option>
												<?php $existe = true; ?>
												@else
												<option  value="{{ $genero->id }}">{{ $genero->nombre }}</option>
												@endif
												@endforeach
												@if($existe == false)
												<option selected="selected"  value="">-- Seleccione un sub-genero --</option>
												@endif
											</select>
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Serie</label>
										<div class="col-sm-9">
											<input type="text" name="serie" class="form-control" value="{{ $libro->serie }}" >
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Disponible</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" value="<?php if ($libro->disponible) echo "Si"; else echo "No"; ?>" readonly>
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Creado</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" value="{{ $libro->created_at->diffForHumans().' ('.$libro->created_at.')' }}" readonly>
										</div>
									</div>

									<div class="form-group form-group-sm">
										<label class="col-sm-3 control-label">Actualizado</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" value="{{ $libro->updated_at->diffForHumans().' ('.$libro->updated_at.')' }}" readonly>
											<span class="help-block">Cuando se realizan prestamos se actualiza el estado del libro.</span>
										</div>
									</div>

									<br>
									
									<div class="">
										<div class="col-sm-offset-3 col-sm-9">
											<div class="btn-group btn-group-justified" role="group" aria-label="...">
												<div class="btn-group" role="group">
													<button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp; Actualizar</button>
												</div>
												<div class="btn-group" role="group">
													<a data-toggle="modal" data-target="#modal-elimina-libro" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Eliminar</a>
												</div>
											</div>
										</div>
									</div>
								</form>
							<br><br><br><br>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-7">
								<h4 class="text-center">Historial de prestamos</h4>
								<div class="table-responsive" style="max-height: 470px; ">
									<table class="table">
										<thead>
											<th>Nombre</th>
											<th>Fecha de retiro</th>
											<th>Fecha de ingreso</th>
										</thead>
										<tbody>
											@if(count($libro->prestamos) > 0)
											<?php $prestamosTemp =  $libro->prestamos->sortByDesc('id');?>
											@foreach($prestamosTemp as $prestamo)
											<tr class="@if($prestamo->updated_at == null) danger @endif" >
												<td>{{ $prestamo->nombre.' '.$prestamo->apellido }}</td>
												<td>{{ $prestamo->created_at }}</td>
												<td>
												@if($prestamo->updated_at != null)
												{{ $prestamo->updated_at->diffForHumans().' ('.$prestamo->updated_at.')' }}
												@else 
												No se ha regresado
												@endif
												</td>
											</tr>
											@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@else 
			<div class="container" >
				<div class="row">
					<div class="col-xs-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="intro-message text-center">
									<h3>Sin datos</h3>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection

@section('modals')

@include('layouts.partials.modal-eliminar-libro')

@endsection
 