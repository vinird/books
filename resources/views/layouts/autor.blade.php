<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
			@if(isset($autor))
			<div class="panel panel-default">
				<div class="panel-heading">
					Información del autor
				</div>
				<div class="panel-body">
					<div class="col-xs-12 ">
						<form class="form-horizontal" autocomplete="off" action="{{ url('admin/autor/update') }}" method="POST">

							{{ csrf_field() }}
							<input type="text" class="hidden" value="{{ $autor->id }}" name="id">


							<div class="form-group form-group-sm">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-9">
									<input type="text" name="nombre" class="form-control" value="{{ $autor->nombre_autor }}" required="" >
								</div>
							</div> 

							<div class="form-group form-group-sm">
								<label class="col-sm-3 control-label">Apellido</label>
								<div class="col-sm-9">
									<input type="text" name="apellido" class="form-control" value="{{ $autor->apellido_autor }}" required="" >
								</div>
							</div> 

							<div class="form-group form-group-sm">
								<label class="col-sm-3 control-label">Libros</label>
								<div class="col-sm-9">
									<?php $cantidad_libros =   $autor->libros->count(); ?>
									<div class="input-group">
										<input type="text" class="form-control" value="{{ $cantidad_libros }}" readonly>
										<span class="input-group-btn">
											<a href="{{ url('admin/buscar_libros_autor/'.$autor->id) }}" class="btn btn-default btn-sm" type="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
										</span>
									</div><!-- /input-group -->
								</div>
							</div> 

							<div class="form-group form-group-sm">
								<label class="col-sm-3 control-label">Creado</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="{{ $autor->created_at->diffForHumans().' ('.$autor->created_at->format('Y h:i:s A').')' }}" readonly>
								</div>
							</div> 

							<div class="form-group form-group-sm">
								<label class="col-sm-3 control-label">Actualizado</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" value="{{ $autor->updated_at->diffForHumans().' ('.$autor->updated_at->format('Y h:i:s A').')' }}" readonly>
								</div>
							</div> 

							<div class="">
								<div class="col-sm-offset-3 col-sm-9">
									<div class="btn-group btn-group-justified" role="group" aria-label="...">
										<div class="btn-group" role="group">
											<button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp; Actualizar</button>
										</div>
										<div class="btn-group" role="group">
											<a data-toggle="modal" data-target="#modal-elimina-autor" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Eliminar</a>
										</div>
									</div>
								</div>
							</div>
						</form>
						<br><br><br>
					</div>
				</div>
				
				@else 

				<div class="panel panel-default">
					<div class="panel-body">
						<div class="intro-message text-center">
							<h3>Sin datos</h3>
						</div>
					</div>
				</div>

				@endif
			</div>
		</div>
	</div>
</div>
@endsection

@section('modals')
@include('layouts.partials.modal-eliminar-autor')
@endsection
