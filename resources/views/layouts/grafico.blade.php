<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<div class="panel panel-default">
				<div class="panel-heading">
					Gráficos 
					<span class="pull-right">Tiempo de ejecución: {!! $data_micro_time !!}s</span>
				</div>
				<div class="panel-body">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="panel panel-info">
							<div class="panel-heading text-center">
								Palabras más buscadas
							</div>
							<div class="panel-body">
								<canvas id="myChart" width="400" height="400" class=""></canvas>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="panel panel-info">
							<div class="panel-heading text-center">
								Géneros más populares
							</div>
							<div class="panel-body">
								<canvas id="busqueda_generos" width="400" height="400" class=""></canvas>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="panel panel-info">
							<div class="panel-heading text-center">
								Libros más prestados
							</div>
							<div class="panel-body">
								<canvas id="libros_prestamos" width="400" height="400" class=""></canvas>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="panel panel-info">
							<div class="panel-heading text-center">
								Prestamos por géneros
							</div>
							<div class="panel-body">
								<canvas id="libors_generos_prestamos" width="400" height="400" class=""></canvas>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 hidden-xs">
						<div class="panel panel-success">
							<div class="panel-heading text-center">
								Cantidad de busquedas y prestamos de los últimos 12 meses
							</div>
							<div class="panel-body">
								<canvas id="data_busquedas_ano" width="400" height="60" class=""></canvas>
							</div>
						</div>
					</div>

				</div>
				

				<div class="panel panel-default">
					<div class="panel-body">
						<div class="text-center">
							<h3>Gráficos de datos del sistema</h3>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@section('modals')
@endsection

@section('custom-scripts')
<script src="{{ asset('js/chartjs/chartjs.min.js') }}"></script>
@include('layouts.partials.scripts-graficos')
@endsection