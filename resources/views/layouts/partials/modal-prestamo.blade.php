<!-- Modal -->
<div class="modal fade" id="modal-prestar-libro" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal-cargar-info-label">Prestar libro</h4>
			</div>
			<form class="form-horizontal" action="{{ url('admin/prestamo/prestar') }}" method="POST" autocomplete="off">
				<div class="modal-body">

					<div class="text-center">
						<strong id="libro_titulo_prestar">nombre libro</strong><br>
					</div>
					<hr>
					{{ csrf_field() }}
					<input type="text" name="id" class="hidden" id="libro_id_prestamo" >

					<div class="text-center">
						<span class="help-block">Para prestar libros es necesario incluir los datos de la persona a quien se le prestará.</span>
						
					</div> 
					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Nombre</label>
						<div class="col-sm-9">
							<input required name="nombre" type="text" class="form-control" maxlength="20">
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Apellido</label>
						<div class="col-sm-9">
							<input required name="apellido" type="text" class="form-control" maxlength="40">
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Teléfono</label>
						<div class="col-sm-9">
							<input required name="telefono" type="text" class="form-control" maxlength="20" pattern="\d+">
							<span class="help-block">Ingrese únicamente números.</span>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-info btn-sm">Prestar</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-devolver-libro" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal-cargar-info-label">Regresar libro</h4>
			</div>
			<form class="form-horizontal" action="{{ url('admin/prestamo/regresar') }}" method="POST" autocomplete="off">
				<div class="modal-body">

					<div class="">

					</div>
					<label class="col-sm-3 text-right">Libro: </label>
					<div class="col-sm-9">
						<span id="libro_titulo_devolver">nombre libro</span><br>
					</div>
					<div class="clearfix"></div>
					<label class="col-sm-3 text-right">Prestado a:</label>
					<div class="col-sm-9">
						<span id="libro_titulo_devolver_persona">Persona</span>
					</div>
					<div class="clearfix"></div>

					<label class="col-sm-3 text-right">Fecha:</label>
					<div class="col-sm-9">
						<span id="libro_titulo_devolver_fecha">Persona</span>
					</div>
					<div class="clearfix"></div>

					{{ csrf_field() }}
					<input type="text" name="id_libro" class="hidden" id="libro_id_prestamo_devolver" ">
					<input type="text" name="id_prestamo" class="hidden" id="prestamo_id_devolver" ">
					<hr>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Contraseña</label>
						<div class="col-sm-9">
							<input required name="contrasena" type="password" class="form-control" >
							<span class="help-block">Debe ingresar su contraseña para regresar el libro.</span>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-info btn-sm">Regresar</button>
				</div>
			</form>
		</div>
	</div>
</div>