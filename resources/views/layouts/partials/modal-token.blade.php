<!-- Modal -->
<div class="modal fade" id="modal-token" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal-cargar-info-label">Token</h4>
				<span class="help-block">El token se utiliza para que nuevos usuarios puedan registrarse en el sistema.</span>
			</div>
			<form class="form-horizontal" action="{{ url('admin/token/update') }}" method="POST" autocomplete="off">
				<div class="modal-body">
					{{ csrf_field() }}

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Token</label>
						<div class="col-sm-9">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="sizing-addon_password"><i class="fa fa-shield" aria-hidden="true"></i></span>
								<input type="text" name="token" class="form-control" aria-describedby="sizing-addon_password" value="{{ michaels_decrypt($token->token) }}" readonly="">
							</div>
							<span class="help-block">El token solo puede ser utilizado para registrar un usuario a la vez.</span>
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Contraseña</label>
						<div class="col-sm-9">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="sizing-addon_password"><i class="fa fa-key" aria-hidden="true"></i></span>
								<input type="password" name="contrasena" class="form-control" aria-describedby="sizing-addon_password" required="">
							</div>
							<span class="help-block">Debe ingresar su contraseña para renovar el token.</span>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Cerrar</button>
					<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp; Renovar token</button>
				</div>
			</form>
		</div>
	</div>
</div> 