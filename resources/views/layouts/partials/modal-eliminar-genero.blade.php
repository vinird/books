<!-- Modal -->
<div class="modal fade" id="modal-elimina-genero" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="modal-cargar-info-label">Eliminar género</h4>
			</div>
			<form class="form-horizontal" action="{{ url('admin/genero/delete') }}" method="POST">
				<div class="modal-body">
					<div class="text-center">
						@if($cantidad_libros > 0)
						<h5>No se puede eliminar el género <strong>{{ $genero->nombre }}</strong> porque tiene libros asignados.</h5>
						@else 
						<h5>¿Desea aliminar el género <strong>{{ $genero->nombre }}</strong>?</h5>
						@endif
					</div>
					<br>
					{{ csrf_field() }}
					<input type="text" name="id" class="hidden" value="{{ $genero->id }}">

					@if($cantidad_libros == 0)
					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Contraseña</label>
						<div class="col-sm-9">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="sizing-addon_password"><i class="fa fa-key" aria-hidden="true"></i></span>
								<input type="password" name="contrasena" class="form-control" aria-describedby="sizing-addon_password">
							</div>
							<span class="help-block">Debe ingresar su contraseña para eliminar el genero.</span>
						</div>
					</div>
					@endif

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Cerrar</button>
					@if($cantidad_libros == 0)
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Eliminar</button>
					@endif
				</div>
			</form>
		</div>
	</div>
</div> 