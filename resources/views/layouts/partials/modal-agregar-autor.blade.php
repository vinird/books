<!-- Modal -->
<div class="modal fade" id="modal-agregar-autor" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="modal-cargar-info-label">Agregar autor</h4>
      </div>
      <form class="form-horizontal" action="{{ url('admin/autor/add') }}" method="POST" autocomplete="off">
        <div class="modal-body">
          {{ csrf_field() }}


          <div class="form-group form-group-sm">
            <label class="col-sm-3 control-label">Nombre</label>
            <div class="col-sm-9">
              <input required name="nombre" type="text" class="form-control" maxlength="20">
            </div>
          </div>

          <div class="form-group form-group-sm">
            <label class="col-sm-3 control-label">Apellido</label>
            <div class="col-sm-9">
              <input required name="apellido" type="text" class="form-control" maxlength="40">
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar</button>
          <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Agregar</button>
        </div>
      </form>
    </div>
  </div>
</div>