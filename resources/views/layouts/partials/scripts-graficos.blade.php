<script>
    // var colors = [
    // '#438742',
    // '#3F5D6A',
    // '#3C4356',
    // '#CF962F',
    // '#B1863C',
    // ]; 
    var colors = [
    '#04BFBF',
    '#CAFCD8',
    '#F7E967',
    '#A9CF54',
    '#588F27',
    ]; 

    // Chartjs globals
    Chart.defaults.global.animation.duration = 2000;
    Chart.defaults.global.animation.easing = 'easeInOutCubic';

    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
            @if(isset($data_busquedas))
                @foreach($data_busquedas as $busqueda)
                '{{ $busqueda->texto }}',
                @endforeach
            @endif
            ],
            datasets: [{
                label: 'Busquedas',
                data: [
                @if(isset($data_busquedas))
                    @foreach($data_busquedas as $busqueda)
                    {{ $busqueda->cantidad }},
                    @endforeach
                @endif
                ],
                backgroundColor: colors,
                // borderColor: [
                // 'rgba(255,99,132,1)',
                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
                // ],
                borderWidth: 0
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    gridLines : {
                        display : true,
                    },
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    gridLines : {
                        display : false,
                    },
                }]
            }
        },
    });

    var busqueda_generos = document.getElementById("busqueda_generos");
    var myChart2 = new Chart(busqueda_generos, {
        type: 'pie',
        data: {
            labels: [
            @if(null !== $data_generos_busquedas)
                @foreach($data_generos_busquedas as $busqueda)
                '{{ $busqueda->nombre }}',
                @endforeach
            @endif
            ],
            datasets: [{
                data: [
                @if(null !== $data_generos_busquedas)
                    @foreach($data_generos_busquedas as $busqueda)
                    {{ $busqueda->busquedas }},
                    @endforeach
                @endif
                ],
                backgroundColor: colors,
                // borderColor: [
                // 'rgba(255,99,132,1)',
                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
                // ],
                borderWidth: 0
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    gridLines : {
                        display : false,
                    },
                }]
            },
        }
    });

    var libros_prestamos = document.getElementById("libros_prestamos");
    var myChart3 = new Chart(libros_prestamos, {
        type: 'doughnut',
        data: {
            labels: [
            @if(isset($data_libros_prestamos))
                @foreach($data_libros_prestamos as $libro)
                    @foreach($data_libros_prestamos as $prestamo)
                        @if(isset($libro->id) && isset($prestamo->libro_id))
                            @if($libro->id == $prestamo->libro_id)
                                '{{ $libro->titulo }}',
                            @endif
                        @endif
                    @endforeach
                @endforeach
            @endif
            ],
            datasets: [{
                label: '# of Votes',
                data: [
                @if(isset($data_libros_prestamos))
                    @foreach($data_libros_prestamos as $prestamo)
                        @foreach($data_libros_prestamos as $libro)
                            @if(isset($libro->id) && isset($prestamo->libro_id))
                                @if($libro->id == $prestamo->libro_id)
                                    '{{ $prestamo->prestamos_count }}',
                                @endif
                            @endif
                        @endforeach
                    @endforeach
                @endif
                ],
                backgroundColor: colors,
                // borderColor: [
                // 'rgba(255,99,132,1)',
                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
                // ],
                borderWidth: 0
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },

                    gridLines : {
                        display : false,
                    },
                }]
            }
        }
    });
    var libors_generos_prestamos = document.getElementById("libors_generos_prestamos");
    var myChart4 = new Chart(libors_generos_prestamos, {
        type: 'polarArea',
        data: {
            labels: [
            @if(isset($data_prestamos_generos))
                @foreach($data_prestamos_generos as $values)
                    <?php $nombre = ""; ?>
                    @foreach($values as $key)
                        @if($nombre != $key->_genero_nombre)
                            '{{ $key->_genero_nombre }}',
                            <?php $nombre = $key->_genero_nombre; ?>
                        @endif
                    @endforeach
                @endforeach
            @endif
            ],
            datasets: [{
                label: '# of Votes',
                data: [
                @if(isset($data_prestamos_generos))
                    @foreach($data_prestamos_generos as $values)
                        <?php $count = 0; ?>
                        @foreach($values as $key)
                            <?php $count = $count + 1; ?>
                        @endforeach
                        {{ $count }},
                    @endforeach
                @endif
                ],
                backgroundColor: colors,
                // borderColor: [
                // 'rgba(255,99,132,1)',
                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
                // ],
                borderWidth: 0,
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    gridLines : {
                        display : false,
                    },
                }]
            },
            legend: {
                display: false,
            }
        }
    });

    var data_busquedas_ano = document.getElementById("data_busquedas_ano");
    var myChart5 = new Chart(data_busquedas_ano, {
        type: 'line',
        data: {
            labels: [
            
                @foreach($data_meses as $mes)
                    <?php $print = $mes->format('F') ?>
                    '{{ $print }}',
                @endforeach
            
            ],
            datasets: [
            {
                data: [
                @if(isset($data_prestamos_ano))
                    @foreach($data_prestamos_ano as $prestamo)
                        <?php $count = 0; ?>
                        @foreach($prestamo as $key)
                            <?php $count = $count + 1; ?>
                        @endforeach
                        {{ $count }},
                    @endforeach
                @endif
                ],
                backgroundColor: 'rgba(207,150,47,0.5)',
                borderWidth: 2,
                label: "Prestamos",
                // fill: false,
                lineTension: 0.3,
                borderColor: "#B1863C",
                // borderCapStyle: 'butt',
                // borderDash: [1],
                // borderDashOffset: 0.0,
                // borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#000",
                pointBorderWidth: 2,
                pointHoverRadius: 7,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "#00114D",
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // spanGaps: false,
            },
            {
                data: [
                @if(isset($data_busquedas_ano))
                    @foreach($data_busquedas_ano as $values)
                        <?php $count = 0; ?>
                        @foreach($values as $key)
                            <?php $count = $count + 1; ?>
                        @endforeach
                        {{ $count }},
                    @endforeach
                @endif
                ],
                backgroundColor: 'rgba(67,135,66,0.5)',
                borderWidth: 2,
                label: "Busquedas",
                // fill: false,
                lineTension: 0.3,
                borderColor: "#438742",
                // borderCapStyle: 'butt',
                // borderDash: [1],
                // borderDashOffset: 0.0,
                // borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#000",
                pointBorderWidth: 2,
                pointHoverRadius: 7,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "#00114D",
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // spanGaps: false,
            },
            
            
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    gridLines : {
                        display : true,
                    },
                }]
            },
        }
    });
</script>