<!-- Modal -->
<div class="modal fade" id="modal-cargar-info" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="modal-cargar-info-label">Cargar información en la base de datos</h4>
            </div>
            <form class="form-horizontal" action="{{ url('/cargar_info') }}" method="POST" enctype="multipart/form-data" id="form_actualizar_datos_excel" required accept=".xml, .ods, .xls, .xlsx"> 
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group form-group-sm">
                        <label class="col-sm-2 control-label">Archivo</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="file" name="archivo" class="form-control">
                                <span class="input-group-btn">
                                    <a href="{{ url('admin/excel/example') }}" class="btn btn-info btn-sm" type="button"><i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp; Descargar plantilla</a>
                                </span>
                            </div><!-- /input-group -->
                            <span class="help-block">Los formatos aceptados son; xlsx, xls, ods y xml.</span>
                            <span class="help-block">Se recomienda no utilizar signos de acentuación en los campos de la hoja de cálculo.</span>
                        </div>
                    </div>
                    <div class="form-group form-group-sm"> 
                        <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="contrasena" id="inputPassword3" placeholder="Digite su contraseña.." required>
                            <span class="help-block">Necesita ingresar su contraseña para actualizar la base de datos.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Close</button>
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-paper-plane" aria-hidden="true"></i>&nbsp; Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>