<!-- Modal -->
<div class="modal fade" id="modal-agregar-libro" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="modal-cargar-info-label">Agregar libro</h4>
			</div>
			<form class="form-horizontal" action="{{ url('admin/libro/add') }}" method="POST" autocomplete="off">
				<div class="modal-body">
					{{ csrf_field() }}
					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Autor</label>
						<div class="col-sm-9">
							<select class="form-control" required name="autor">
								<option selected="selected"  value="">-- Seleccione un autor --</option>
								@foreach($autors as $autor)
								<option value="{{ $autor->id }}">{{ $autor->apellido_autor.' '.$autor->nombre_autor  }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Título</label>
						<div class="col-sm-9">
							<input required name="titulo" type="text" class="form-control" maxlength="100">
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">ISBN</label>
						<div class="col-sm-9">
							<input required name="isbn" type="text" class="form-control" maxlength="40">
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Género</label>
						<div class="col-sm-9">
							<select class="form-control" required="" name='genero'>
								<option selected="selected"  value="">-- Seleccione un género --</option>
								@foreach($generos as $genero)
								<option value="{{ $genero->id }}">{{ $genero->nombre }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Sub-género</label>
						<div class="col-sm-9">
							<select class="form-control" name="subgenero">
								<option selected="selected"  value="">-- Seleccione un sub-genero --</option>
								@foreach($generos as $genero)
								<option value="{{ $genero->id }}">{{ $genero->nombre }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Serie</label>
						<div class="col-sm-9">
							<input name="serie" type="text" class="form-control" pattern="[0-9]">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Cerrar</button>
					<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Agregar</button>
				</div>
			</form>
		</div>
	</div>
</div>