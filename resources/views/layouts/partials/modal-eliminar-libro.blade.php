<!-- Modal -->
<div class="modal fade" id="modal-elimina-libro" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="modal-cargar-info-label">Eliminar libro</h4>
			</div>
			<form class="form-horizontal" action="{{ url('admin/libro/delete') }}" method="POST">
				<div class="modal-body">
					<div class="text-center">
						<h5>¿Desea aliminar el libro <strong>{{ $libro->titulo }}</strong>? <br> Esta acción eliminará todos los datos de prestamos de este libro.</h5>
					</div>
					<br>
					{{ csrf_field() }}
					<input type="text" name="id" class="hidden" value="{{ $libro->id }}">

					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Contraseña</label>
						<div class="col-sm-9">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="sizing-addon_password"><i class="fa fa-key" aria-hidden="true"></i></span>
								<input type="password" name="contrasena" class="form-control" aria-describedby="sizing-addon_password">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Cerrar</button>
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>