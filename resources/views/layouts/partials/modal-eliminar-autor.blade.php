 <!-- Modal -->
<div class="modal fade" id="modal-elimina-autor" tabindex="-1" role="dialog" aria-labelledby="modal-cargar-info-label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="modal-cargar-info-label">Eliminar autor</h4>
			</div>
			<form class="form-horizontal" action="{{ url('admin/autor/delete') }}" method="POST" autocomplete="off">
				<div class="modal-body">
					<div class="text-center">
						@if($cantidad_libros > 0)
						<h5>No se puede eliminar el autor <strong>{{ $autor->apellido_autor.' '.$autor->nombre_autor }}</strong> porque tiene libros asignados.</h5>
						@else 
						<h5>¿Desea aliminar el autor <strong>{{ $autor->apellido_autor.' '.$autor->nombre_autor }}</strong>?</h5>
						@endif
					</div>
					<br>
					{{ csrf_field() }}
					<input type="text" name="id" class="hidden" value="{{ $autor->id }}">

					@if($cantidad_libros == 0)
					<div class="form-group form-group-sm">
						<label class="col-sm-3 control-label">Contraseña</label>
						<div class="col-sm-9">
							<div class="input-group input-group-sm">
								<span class="input-group-addon" id="sizing-addon_password"><i class="fa fa-key" aria-hidden="true"></i></span>
								<input type="password" name="contrasena" class="form-control" aria-describedby="sizing-addon_password">
							</div>
							<span class="help-block">Debe ingresar su contraseña para eliminar el autor.</span>
						</div>
					</div>
					@endif

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Cerrar</button>
					@if($cantidad_libros == 0)
					<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Eliminar</button>
					@endif
				</div>
			</form>
		</div>
	</div>
</div> 