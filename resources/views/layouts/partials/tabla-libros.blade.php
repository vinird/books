@if(isset($libros))
<div class="panel panel-success">
	<div class="panel-heading">Libros</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table id="tabla_libros" class="table table-hover">
				<thead>
					<th>Autor</th>
					<th>Título</th>
					<th>Serie</th>
					<th>ISBN</th>
					<th>Género</th>
					<th>Sub-genero</th>
					<th>Disponible</th>
					@if($admin && auth()->check())
					<th></th>
					@endif
				</thead>
				<tbody>
					@foreach($libros as $libro)
					<tr class="<?php if(!$libro->disponible) echo "danger";?>">
						<td> 
							@if(isset($libro->autor)) 
							@if(auth()->check())
							<a href="{{ url('admin/buscar_autor/'.$libro->autor->id) }}">
								{{ $libro->autor->apellido_autor.' '.$libro->autor->nombre_autor }} 	
							</a>
							@else
								{{ $libro->autor->apellido_autor.' '.$libro->autor->nombre_autor }} 	
							@endif
							@endif 
						</td>
						<td>{{ $libro->titulo }}</td>
						<td>{{ $libro->serie }}</td>
						<td>{{ $libro->isbn }}</td>
						<td><?php if(isset($libro->genero)) echo $libro->genero->nombre; ?></td>
						<td><?php if(isset($libro->sub_genero)) echo $libro->sub_genero->nombre; ?></td>
						<td><?php if(!$libro->disponible) echo "No"; else echo "Si";?></td>
						@if($admin && auth()->check())
						<td>
							<a href="{{ url('admin/libro/'.$libro->id) }}" class="btn btn-default btn-xs">Ver</a>
							@if(!$libro->disponible)
							<?php 
							$ultimo_prestamo = $libro->prestamos->sortByDesc('id')->first(); 
							$nombre_ultimo_prestamo = $ultimo_prestamo->nombre.' '.$ultimo_prestamo->apellido;
							$fecha_prestamo = $ultimo_prestamo->created_at->diffForHumans().' ('.$ultimo_prestamo->created_at.')';
							$id_prestamo = $ultimo_prestamo->id;
							?>
							<a class="btn btn-info btn-xs" onclick="devolver_libro('{!! $libro->titulo !!}'+' ('+'{{ $libro->isbn }}'+') ', {{ $libro->id }} , '{{ $nombre_ultimo_prestamo }}' , '{{ $fecha_prestamo }}' , '{{ $id_prestamo }}')">Regresar</a>
							@else
							<a class="btn btn-default btn-xs" onclick="prestar_libro('{!! $libro->titulo !!}'+' ('+'{{ $libro->isbn }}'+') ', {{ $libro->id }})">Prestar</a>
							@endif
						</td>
						@endif
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

	</div>
</div>
@endif

@if(auth()->check())

@include('layouts.partials.modal-prestamo')

@endif