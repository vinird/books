 <!-- Footer -->
 <div class="footer-before"></div>
 <footer>
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12">
          <ul class="list-inline">
           <li>
            <a href="/">Biblioteca</a>
         </li>
         <li class="footer-menu-divider">&sdot;</li>
         <li>
            <a href="/home">Inicio</a>
         </li>
         @if(auth()->check())
         @if(auth()->user()->activo)
         <li class="footer-menu-divider">&sdot;</li>
         <li>
            <a href="{{ url('admin/graficos') }}">Gráficos</a>
         </li>
         @endif
         @endif
      </ul>
      <p class="copyright text-muted small">Copyright &copy; Your Company 2014. All Rights Reserved</p>
   </div>
</div>
</div>
</footer>

@if(Session::has('alert'))
<script type="text/javascript">
 var type = "{!! session()->get('type') !!}";
 var text = "{!! session()->get('alert') !!}";
 switch (type) {
  case 'success':
  toastr.success(text);
  break;
  case 'warning':
  toastr.warning(text);
  break;
  case 'error':
  toastr.error(text);
  break;
  case 'info':
  toastr.info(text);
  break;
  default:
			// statements_def
			break;
      }

   </script>
   <?php session()->forget('alert'); ?>
   @endif

   @if (count($errors) > 0)
   @foreach ($errors->all() as $error)
   <script>
    toastr.error("{{ $error }}");
 </script>
 @endforeach
 @endif

 <script>
    $(document).ready( function () {
     $('#tabla_libros').DataTable({
      dom: 'Bfrtip',
      buttons: [
      'print'
      ],
      "language": {
       "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "sUrl":            "",
       "sInfoThousands":  ",",
       "sLoadingRecords": "Cargando...",
       "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
     },
     "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
     }
  },
});
     $('#tabla_prestamos').DataTable({
      "order": [[ 1, "desc" ]],
      dom: 'Bfrtip',
      buttons: [
      'print'
      ],
      "language": {
       "sProcessing":     "Procesando...",
       "sLengthMenu":     "Mostrar _MENU_ registros",
       "sZeroRecords":    "No se encontraron resultados",
       "sEmptyTable":     "Ningún dato disponible en esta tabla",
       "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
       "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
       "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
       "sInfoPostFix":    "",
       "sSearch":         "Buscar:",
       "sUrl":            "",
       "sInfoThousands":  ",",
       "sLoadingRecords": "Cargando...",
       "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
     },
     "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
     }
  },
});
  } );
</script>