<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-warning">
				<div class="panel-heading">Prestamos</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table" id="tabla_prestamos">
							<thead>
								<th>Nombre</th>
								<th>Fecha de retiro</th>
								<th>Fecha de ingreso</th>
								<th>Libro</th>
							</thead>
							<tbody>
								@if(isset($prestamos))
								@foreach($prestamos as $prestamo)
								<tr class="@if($prestamo->updated_at == null) danger @endif" >
									<td>{{ $prestamo->nombre.' '.$prestamo->apellido }}</td>
									<td>{{ $prestamo->created_at }}</td>
									<td>
										@if($prestamo->updated_at != null)
										{{ $prestamo->updated_at->diffForHumans().' ('.$prestamo->updated_at.')' }}
										@else 
										No se ha regresado
										@endif
									</td>
									<td><a href="{{ url('admin/libroUnitario/'.$prestamo->libro->id) }}">{{ $prestamo->libro->titulo }}</a></td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
