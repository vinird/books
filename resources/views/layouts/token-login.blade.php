@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<div class="panel panel-primary">
				<div class="panel-heading">Ingreso de token</div>
				<div class="panel-body">
					<form action="{{ url('admin/token/check') }}" method="POST" autocomplete="off">
						{{ csrf_field() }}
						<label class="control-label">Token</label>
						<div class="input-group input-group-sm">
							<span class="input-group-addon" id="sizing-addon_password"><i class="fa fa-shield" aria-hidden="true"></i></span>
							<input type="text" name="token" class="form-control" aria-describedby="sizing-addon_password" required="">
						</div>
						<span class="help-block">Si el token es incorrecto puede solicitar acceso al administrador del sistema, quien puede habilitarlo como usuario o enviarle un nuevo token de acceso.</span>
						<br>
						<div class="pull-right">
							@if(auth()->user()->solicitud == true)
							<a class="btn btn-success btn-sm" disabled>Acceso solicitado </a> &nbsp; &nbsp;
							@else
							<a href="{{ url('admin/users/request_access') }}" class="btn btn-default btn-sm"><i class="fa fa-exclamation" aria-hidden="true"></i>&nbsp; Solicitar acceso </a> &nbsp; &nbsp;
							@endif

							<button class="btn btn-primary btn-sm"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp; Enviar token para comprobación </button>
							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
