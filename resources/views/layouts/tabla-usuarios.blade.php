<?php 
$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Usuarios colaboradores</div>
				<div class="panel-body">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1" style="min-height: 200px;">
						@if(isset($users))
						@if(count($users))
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>Estado</th>
									<th>Nombre</th>
									<th>E-mail</th>
									<th></th>
									<th></th>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr class="@if($user->nuevo || $user->solicitud) info @endif">
										<td>
											<?php 
											if ($user->block) 
											{
												echo '<span class="label label-danger"><I>Bloqueado</I></span>';
											}
											elseif($user->activo)
											{
												echo '<span class="label label-success">Activo</span>';
											}
											else
											{
												echo '<span class="label label-default"><I>Inactivo</I></span>';
											}
											?>
										</td>
										<td>{{ $user->name }}</td>
										<td>{{ $user->email }}</td>
										<td class="text-center">
											@if($user->solicitud)
											<span class="text-warning">Solicitud de acceso</span><br>
											<a href="{{ url('admin/users/enable/'.$user->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Aceptar</a>

											<a href="{{ url('admin/users/block/'.$user->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle" aria-hidden="true"></i> &nbsp;Rechazar</a>
											@endif
										</td>
										<td class="text-right">
											<?php 
											if ($user->block) 
											{
												echo ' <a href="'.url('admin/users/unblock/'.$user->id).'" class="btn btn-default btn-xs"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> &nbsp;Desbloquear</a> ';
											}
											else
											{
												echo ' <a href="'.url('admin/users/block/'.$user->id).'" class="btn btn-default btn-xs"><i class="fa fa-ban" aria-hidden="true"></i> &nbsp;Bloquear</a> ';
												if ($user->activo) 
												{
													echo ' <a href="'.url('admin/users/disable/'.$user->id).'" class="btn btn-default btn-xs"><i class="fa fa-minus-circle" aria-hidden="true"></i> &nbsp;Desactivar</a> ';
													# code...
												}
												else
												{
													echo ' <a href="'.url('admin/users/enable/'.$user->id).'" class="btn btn-default btn-xs"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Activar</a> ';

												}
											}
											?>
											
											<a href="{{ url('admin/users/delete/'.$user->id) }}" data-ready="false" class="btn btn-default btn-xs btn-delete-user"><i class="fa fa-trash" aria-hidden="true"></i> &nbsp;Eliminar</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						@else 
						<div class="text-center">
							<h3>No hay colaboradores registrados</h3>
						</div>
						@endif
						@endif

					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('custon-scripts')

@endsection
