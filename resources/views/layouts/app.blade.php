<?php 
$user_notification = user_notification();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('img/book-ico.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Biblioteca</title>
    
    <!-- Fontawesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

    @include('layouts.partials.welcome-style')

    <!-- Styles -->
    <link href="/css/app.min.css" rel="stylesheet">

    <!-- Toastr -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr/toastr.min.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables/responsive.bootstrap.min.css') }}">

    <!-- Custom -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <style type="text/css"> 
        body,
        html {
            width: 100%;
            height: 100%;
            background-color: #ECF0F5!important;
        }
    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/home') }}">
                            Inicio 
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            @if(Auth::check() && auth()->user()->activo)
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-book" aria-hidden="true"></i>&nbsp; Libros <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#modal-agregar-libro"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Agregar</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/libros_all') }}"><i class="fa fa-eye" aria-hidden="true"></i> &nbsp;Ver todos</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp; Géneros <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#modal-agregar-genero"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Agregar</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/generos') }}"><i class="fa fa-eye" aria-hidden="true"></i> &nbsp;Ver todos</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-handshake-o" aria-hidden="true"></i>&nbsp; Prestamos <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('admin/prestamos/1') }}"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp; Ver prestamos del mes</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/prestamos/2') }}"><i class="fa fa-eye" aria-hidden="true"></i> &nbsp;Ver todos</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-user" aria-hidden="true"></i>&nbsp; Autores <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#modal-agregar-autor"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Agregar</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/autores') }}"><i class="fa fa-eye" aria-hidden="true"></i> &nbsp;Ver todos</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('admin/graficos') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp; Gráficos</a>
                            </li>
                            @endif
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Ingresar</a></li>
                            <li><a href="{{ url('/register') }}">Registrarse</a></li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    @if(auth()->user()->admin)
                                    @if($user_notification > 0)
                                    <i class="fa fa-flag" style="color: red;" aria-hidden="true"></i>&nbsp; 
                                    @endif
                                    @endif
                                    <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; 
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    @if(auth()->user()->activo)
                                    <li>
                                        <a href="{{ url('admin/excel/export') }}">
                                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp; Obtener Excel de la base de datos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#modal-cargar-info">
                                            <i class="fa fa-database" aria-hidden="true"></i>&nbsp; Cargar base de datos
                                        </a>
                                    </li>
                                    @endif
                                    @if(auth()->user()->admin)
                                    <li class="divider"></li>
                                    <li class="dropdown-header">Sección del administrador</li>
                                    <li>
                                        <a href="{{ url('admin/users') }}">
                                            <i class="fa fa-users" aria-hidden="true"></i>&nbsp; Usuarios &nbsp;
                                            @if(auth()->user()->admin)
                                            @if($user_notification > 0)
                                            <i class="fa fa-exclamation-circle" style="color: red;" aria-hidden="true"></i> 
                                            @endif
                                            @endif
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#modal-token">
                                            <i class="fa fa-shield" aria-hidden="true"></i>&nbsp; Token
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    @endif
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Salir
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>


    <!-- Modales -->
    @if(auth()->check() && auth()->user()->activo)
    @yield('modals')

    @include('layouts.partials.modal-cargar-info')
    @include('layouts.partials.modal-agregar-libro')
    @include('layouts.partials.modal-agregar-genero')
    @include('layouts.partials.modal-agregar-autor')
    @include('layouts.partials.modal-token')

    @endif

    <!-- Scripts -->
    <script src="/js/app.js"></script>

    <!-- Toastr -->
    <script src="{{ asset('js/toastr/toastr.min.js') }}"></script>

    <!-- DataTables -->
    <script src="{{ asset('js/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/responsive.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/buttons.print.min.js') }}"></script>

    <!-- Custom -->
    <script src="{{ asset('js/custom-app.js') }}"></script>

    @yield('custom-scripts')


    @include('layouts.footer')
</body>
</html>
