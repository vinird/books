<?php $admin = false; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('img/book-ico.png') }}">

    <title>Biblioteca</title>

    <!-- Fontawesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

    @include('layouts.partials.welcome-style')

    <link rel="stylesheet" type="text/css" href="{{ url('css/app.min.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables/responsive.bootstrap.min.css') }}">
    <style type="text/css">
       body,
       html {
        width: 100%;
        height: 100%;
        background-color: #2B2929!important;
    }
</style> 
</head>
<body>
    <div id="app">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top topnav" role="navigation">
            <div class="container topnav">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand topnav" href="/"><i class="fa fa-book" aria-hidden="true"></i> &nbsp;Biblioteca</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form navbar-right input-group" method="POST" action="{{ url('/buscar_libro') }}">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form-control" name="text" placeholder="Escriba el autor o título..." required maxlength="100">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </span>
                        </div><!-- /input-group -->
                    </form>

                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>


        <!-- Header -->
        <a name="about"></a>
        <div class="intro-header">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        @if(isset($libros))
                        <div class="intro-message text-justify" style="color: #5D5A5A; padding-top: 8%;">
                            @include('layouts.partials.tabla-libros')
                        </div>
                        @else 
                        <div class="intro-message">
                            <h1>Biblioteca</h1>
                            <h3>ADISL</h3>
                            <hr class="intro-divider">
                            <ul class="list-inline intro-social-buttons">
                                @if(isset($generos))
                                @foreach($generos as $genero)
                                <li>
                                    <a href="{{ url('/buscar_libro_genero/'.$genero->id) }}" class="btn btn-default btn-lg"><span class="network-name">{{ $genero->nombre }}</span></a>
                                </li>
                                @endforeach
                                <br><br>
                                @endif
                                <form method="POST" action="{{ url('/buscar_libro') }}">
                                    {{ csrf_field() }}
                                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="text" placeholder="Escriba el autor o título..." required maxlength="100">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i>&nbsp; Buscar</button>
                                            </span>
                                        </div><!-- /input-group -->
                                    </div><!-- /.col-lg-6 -->
                                </form>
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>

            </div>
            <!-- /.container -->

        </div>
        <!-- /.intro-header -->
    </div>

    <!-- Footer -->
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="#about">Biblioteca</a>
                        </li>
                        @if (Route::has('login'))
                        @if(auth()->check())
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="/home">Inicio</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="{{ url('/home') }}">{{ auth()->user()->name }}</a>
                        </li>
                        @else
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="{{ url('/login') }}">Ingresar</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="{{ url('/register') }}">Registrarse</a>
                        </li>
                        @endif 
                        @endif
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; Your Company 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- DataTables -->
    <script src="{{ asset('js/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/responsive.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready( function () { 
            $('#tabla_libros').DataTable({
                dom: 'Bfrtip',
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>

</body>
</html>
