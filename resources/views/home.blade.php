<?php 
$admin = true; 

$generos = generos();
$autors  = autores();
$token   = token();
?>
@extends('layouts.app')



@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			@if(isset($libros))
			@include('layouts.partials.tabla-libros')

			@else 
			<div class="container" >
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">
						<div class="panel panel-default">
							<div class="panel-body"> 
							<div class="intro-message text-center">
									<h1>ADISL</h1>
									<h3>Sistema de control de libros</h3>
									<hr class="intro-divider">
									<ul class="list-inline intro-social-buttons">
										@if(isset($generos))
										@foreach($generos as $genero)
										<li>
											<a href="{{ url('admin/buscar_libro_genero/'.$genero->id) }}" class="btn btn-default btn-lg"><span class="network-name">{{ $genero->nombre }}</span></a>
										</li>
										@endforeach
										<br><br>
										@endif
										<form method="POST" action="{{ url('admin/buscar_libro') }}">
											{{ csrf_field() }}
											<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-12">
												<div class="input-group">
													<input type="text" class="form-control" name="text" placeholder="Escriba el autor o título..." required maxlength="100">
													<span class="input-group-btn">
														<button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i>&nbsp; Buscar</button>
													</span>
												</div><!-- /input-group -->
											</div><!-- /.col-lg-6 -->
										</form>
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
