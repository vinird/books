<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index_cliente');


Auth::routes();

Route::get('/home', 'HomeController@index')->middleware(['auth', 'activo']);

// Hoja de cálculo
Route::post('/cargar_info', 'Libros@cargar_info')->middleware(['auth', 'activo']);

// buscar libros
Route::post('/buscar_libro', 'Libros@buscar_libro');
Route::post('admin/buscar_libro', 'Libros@admin_buscar_libro')->middleware(['auth', 'activo']);

Route::get('/buscar_libro_genero/{genero_id}', 'Libros@buscar_libro_genero');
Route::get('admin/buscar_libro_genero/{genero_id}', 'Libros@admin_buscar_libro_genero')->middleware(['auth', 'activo']);
Route::get('admin/libros_all', 'Libros@libros_all')->middleware(['auth', 'activo']);

Route::get('admin/libro/{id}', 'Libros@libro')->middleware(['auth', 'activo']);
Route::get('admin/libroUnitario/{id}', 'Libros@show')->middleware(['auth', 'activo']);

// Agregar libro 
Route::post('admin/libro/add', 'Libros@store')->middleware(['auth', 'activo']);
// Eliminar libro
Route::post('admin/libro/delete', 'Libros@delete')->middleware(['auth', 'activo']);
// Actualizar libro
Route::post('admin/libro/update', 'Libros@update')->middleware(['auth', 'activo']);

// Generos
Route::get('admin/generos', 'Generos@index')->middleware(['auth', 'activo']);
Route::get('admin/genero/{id}', 'Generos@show')->middleware(['auth', 'activo']);
Route::post('admin/genero/add', 'Generos@store')->middleware(['auth', 'activo']);
Route::post('admin/genero/delete', 'Generos@delete')->middleware(['auth', 'activo']);
Route::post('admin/genero/update', 'Generos@update')->middleware(['auth', 'activo']);


// Prestamos
Route::post('admin/prestamo/prestar', 'Prestamos@store')->middleware(['auth', 'activo']);
Route::post('admin/prestamo/regresar', 'Prestamos@regresar')->middleware(['auth', 'activo']);
Route::get('admin/prestamos/{mes}', 'Prestamos@index')->middleware(['auth', 'activo']); // 1 = si es mes, 2 = mostrar todos

// Autores
Route::get('admin/autores', 'Autores@index')->middleware(['auth', 'activo']);
Route::get('admin/buscar_autor/{id}', 'Autores@buscar_autor')->middleware(['auth', 'activo']);
Route::get('admin/buscar_libros_autor/{id_autor}', 'Autores@buscar_libros_autor')->middleware(['auth', 'activo']);
Route::post('admin/autor/add', 'Autores@store')->middleware(['auth', 'activo']);
Route::post('admin/autor/delete', 'Autores@delete')->middleware(['auth', 'activo']);
Route::post('admin/autor/update', 'Autores@update')->middleware(['auth', 'activo']);

// Ver gráficos
Route::get('admin/graficos', 'Graficos@index')->middleware(['auth', 'activo']);

// Token
Route::post('admin/token/update', 'Tokens@update')->middleware(['auth', 'admin']);
Route::post('admin/token/check', 'Tokens@check')->middleware('auth');
Route::get('admin/token/login', function(){
	return view('layouts.token-login');
})->middleware('auth');

// User
Route::get('admin/users', 'Users@index')->middleware(['auth','admin']);
Route::get('admin/users/block/{id}', 'Users@block')->middleware(['auth','admin']);
Route::get('admin/users/unblock/{id}', 'Users@unblock')->middleware(['auth','admin']);
Route::get('admin/users/enable/{id}', 'Users@enable')->middleware(['auth','admin']);
Route::get('admin/users/disable/{id}', 'Users@disable')->middleware(['auth','admin']);
Route::get('admin/users/delete/{id}', 'Users@delete')->middleware(['auth','admin']);

Route::get('admin/users/request_access', 'Users@request_access')->middleware(['auth']);

// Excel
Route::get('admin/excel/export', 'ExcelData@export')->middleware(['auth', 'activo']);
Route::get('admin/excel/example', 'ExcelData@example')->middleware(['auth', 'activo']);



