<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \DB::table('users')->insert([
            'name'     => 'Administrador',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('password'),
            'activo'   => true,
            'admin'     => true,
            'nuevo'     => false,
        	]);

        \DB::table('tokens')->insert([
            'token' => michaels_encrypt(str_random(30)),
            ]);

        // \DB::table('generos')->insert([
        // 	'nombre' => 'Young Adult',
        // 	]);
        // \DB::table('generos')->insert([
        // 	'nombre' => 'Fiction',
        // 	]);
        // \DB::table('generos')->insert([
        // 	'nombre' => "Children's Fiction",
        // 	]);
        // \DB::table('generos')->insert([
        // 	'nombre' => 'Self-help',
        // 	]);
        // \DB::table('generos')->insert([
        // 	'nombre' => 'Non-fiction',
        // 	]);
        // \DB::table('generos')->insert([
        // 	'nombre' => 'Play',
        // 	]);
        // \DB::table('generos')->insert([
        // 	'nombre' => 'Reference',
        // 	]);
    }
}
