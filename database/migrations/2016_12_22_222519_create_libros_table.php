<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 100);
            $table->integer('serie')->nullable();
            $table->string('isbn', 40);

            $table->boolean('disponible')->default(true);

            $table->integer('autor_id')->unsigned()->nullable();
            $table->foreign('autor_id')->references('id')->on('autors');

            $table->integer('genero_id')->unsigned()->nullable();
            $table->integer('sub_genero_id')->unsigned()->nullable();

            $table->foreign('genero_id')->references('id')->on('generos');
            $table->foreign('sub_genero_id')->references('id')->on('generos');

            $table->index('titulo');
            $table->index('isbn');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
    }
}
