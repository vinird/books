jQuery(document).ready(function($) {

	$('#form_actualizar_datos_excel').submit(function(event) {
		$('#modal-cargar-info').modal('toggle');
		toastr.info('Enviando archivo...');
		var final = true;

		setInterval(function(){
			if( alertM.length > 0)
			{
				var posicion = Math.floor((Math.random() * alertM.length));
				toastr.info(alertM[posicion]);
				alertM.splice(posicion, 1)
			} else
			{
				if(alertF.length > 0)
				{
					var posicion = Math.floor((Math.random() * alertF.length));
					toastr.info(alertF[posicion]);
					alertF.splice(posicion, 1)
				}else 
				{
					if (final) 
					{
						toastr.options = {
							"timeOut": "0",
						}
						toastr.info('OK esto definitivamente va a tardar.');
						final = false;
					}	
				}
			}
		},6000);
	});

	$('.btn-delete-user').click(function(event) {
		if ($(this).attr('data-ready') == 'false') 
		{
			$(this).attr('class', 'btn btn-danger btn-xs');
			$(this).attr('data-ready', 'true');
			return false;
		}
	});

});


var alertM = 
[
'Esto puede tardar unos minutos',
'Por favor espere',
'Su archivo se esta procesando',
'La base de datos se esta actualizando',
];
var alertF = 
[
'La paciencia es un don',
'Solo falta poco',
'Debería de actualizar su conexión de internet', 
'Recuerde que se están actualizando los autores, géneros, sub-generos y libros',
'Puede ir a tomar un café mientras espera',
'Puede escuchar música mientras espera',
];


// Prestar libro
function prestar_libro(titulo, libro_id) 
{
	$('#libro_titulo_prestar').text(titulo);
	$('#libro_id_prestamo').val(libro_id);
	$('#modal-prestar-libro').modal('toggle');
}
function devolver_libro(titulo, libro_id, nombre_persona, fecha_prestamo, id_prestamo) 
{
	$('#libro_titulo_devolver_persona').text(nombre_persona);
	$('#libro_titulo_devolver_fecha').text(fecha_prestamo);
	$('#libro_id_prestamo_devolver').val(libro_id);
	$('#prestamo_id_devolver').val(id_prestamo);
	$('#modal-devolver-libro').modal('toggle');	
	$('#libro_titulo_devolver').text(titulo);
}